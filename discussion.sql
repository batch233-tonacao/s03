===================
SQL CRUD OPERATIONS		
===================

1. (Create) Adding a Record
	Terminal
	Adding a Record:
	Syntax
		INSERT INTO table_name (column_name) VALUES (values1);
	Example
		INSERT INTO artists (name) VALUES ("Nirvana");

		-- Mini-Activity:
			-- insert two more artists in the artists table
			-- send your artists table in our batch hangouts

		-- Solution
		INSERT INTO artists (name) VALUES ("Paramore");
		INSERT INTO artists (name) VALUES ("BTS");

2. (Read) Show all records
	Terminal
		Displaying / retrieving records
		Syntax
			SELECT column_name FROM table_name;
		Example
			SELECT name FROM artists;

3. (Create) Adding record with multiple columns
	Terminal
		Adding a record with multiple columns
		Syntax
			INSERT INTO table_name (column_name, column_name) VALUES (value1, value2)
		Example
			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Nevermind", "1991-09-24", 1);

			-- Mini-Activity:
				-- insert albums in your chosen 2 artists
				-- send your albums table in our batch hangouts

			-- Solution
			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("After Laughter", "2017-05-12", 2);
			INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Face Yourself", "2018-04-04", 3);

4. (Create) Adding multiple records
	Terminal
		Adding multiple records
		Syntax
		INSERT INTO table_name (column_name, column_name) VALUES (value1, value2), (value3, value4);
		Example
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Smell Like A Teen Spirit", 501, "Grunge", 1);


		-- Mini-Activity:
			-- insert songs in your chosen 2 albums
			-- send your songs table in our batch hangouts

		-- Solution
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fake Happy", 356, "New Wave", 2);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Don't Leave Me", 346, "Electronic / Kpop", 3);

5. Show records with selected columns
	Terminal
		Retrieving records with selected columns
		Syntax 
		SELECT (column_name1, column_name2) FROM table_name;
		SELECT song_name, genre FROM songs;

		-- Mini-Activity:
			-- retrieve the album title and when it was released
			-- send the output from terminal in our batch hangouts

		-- Solution
		SELECT album_title, date_released FROM albums;

		SELECT * FROM songs;
		-- will show the full table

6. Show records that meet a certain condition
	Terminal
		Retrieving records with certain conditions
	Syntax
		SELECT column_name FROM table_name WHERE condition;
	Example
		SELECT song_name FROM songs WHERE genre = "New Wave";

7. Show records with multiple conditions
	Terminal
		Displaying / retrieving records with multiple conditions
		Syntax
			AND Clause
			SELECT column_name FROM table_name WHERE condition1 AND condition2;

			OR Clause
			SELECT column_name FROM table_name WHERE condition1 OR condition2;

			-- we can use AND and OR keyword for multiple expressions in the WHERE clause

		Example
			SELECT song_name, length FROM songs WHERE length > 314 OR genre = "Grunge";
			-- at least one of the condition was met
			SELECT song_name, length FROM songs WHERE length > 314 AND genre = "Grunge";
			-- all conditions must be met
			SELECT * FROM songs WHERE length > 350 OR genre = "Grunge";


8. (Update) Updating Records
	Terminal

	Add a record to update:
		INSERT INTO artists (name) VALUES ("Incubus"); 
		INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Monuments and Melodies", "2009-06-16", 4);
		INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Megalomaniac", 410, "Classical", 4);

	Updating records:
	Syntax
		UPDATE table_name SET column_name = value WHERE condition;
		UPDATE table_name SET column_name = value, column_name2 = value2 WHERE condition;
		-- when updating or deleting, add a WHERE clause or else you may update or delete all items in a table
		UPDATE songs SET genre = "Rock" WHERE length > 400;
		UPDATE songs SET length = 240 WHERE song_name = "Fake Happy";

9. (Delete) Deleting records
	Terminal
		Deleting records
		Syntax
			DELETE FROM table_name WHERE condition;
			DELETE FROM songs WHERE genre = "ROCK" AND length > 400;
		-- removing where clause deletes all rows

-- Mini-Activity: SOLUTION later at 10:30 AM
--In tasksdb,
	-- Insert 2 users
	-- In tasks table, insert 2 tasks each
	-- Retrieve the users table and tasks table
	-- Send your output in Hangouts
